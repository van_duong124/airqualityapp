import { addIndex, isEmpty, length, map } from "ramda";
import { useState } from "react";
import { Button, Grid } from "semantic-ui-react";
import { SemanticWIDTHS } from "semantic-ui-react/dist/commonjs/generic";
import "./App.css";
import { aqiInfoArr } from "./components/AirQualityInfo/constants";
import { IAqiInfo } from "./components/AirQualityInfo/types";
import { AirQualityPanel } from "./components/AirQualityPanel";
import { BottomAQIInfoBar } from "./components/BottomAQIInfoBar";
import { CitySearchBox } from "./components/CitySearchBox";

const App = () => {
  const [searchInput, setSearchInput] = useState("");

  return (
    <div className="App">
      <Grid
        columns={2}
        verticalAlign="middle"
        centered
        style={{ height: "100%" }}
      >
        <Grid.Row centered style={{ height: "90%" }}>
          {!isEmpty(searchInput) && (
            <Grid.Column width={13} style={{ height: "100%" }}>
              <AirQualityPanel city={searchInput} />
            </Grid.Column>
          )}
          <Grid.Column width={3}>
            <CitySearchBox onSearchComplete={setSearchInput} />
          </Grid.Column>
        </Grid.Row>
      </Grid>

      <BottomAQIInfoBar />
    </div>
  );
};

export default App;
