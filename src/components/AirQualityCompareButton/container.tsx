import { Button } from "semantic-ui-react";

interface IAirQualityCompareButton {
  onCompare: () => void;
  onClearAll: () => void;
}

const AirQualityCompareButton = ({
  onCompare,
  onClearAll,
}: IAirQualityCompareButton) => {
  return (
    <>
      <Button onClick={onCompare}>Compare</Button>
      <Button onClick={onClearAll}>Clear</Button>
    </>
  );
};

export default AirQualityCompareButton;
