import { useState } from "react";
import { DropdownItemProps, Grid, Message } from "semantic-ui-react";
import { IAirQuality, IAQMeasurement } from "../AirQualityInfo/types";
import { CityAreaSelection } from "../CityAreaSelection";
import { PollutantSelection } from "../PollutantSelection";
import usePollutantSelection from "../PollutantSelection/usePollutantSelection";
import useOpenAQ from "./useOpenAQ";
import useDropDown from "../CityAreaSelection/useDropdown";
import { addIndex, isEmpty, map } from "ramda";
import { AirQualityInfo } from "../AirQualityInfo";
import { AirQualityCompareButton } from "../AirQualityCompareButton";
import { AirQualityCompareGraph } from "../AirQualityCompareGraph";
import { MessageBox } from "../MessageBox";

const mapIndexed = addIndex(map);

const AirQualityPanel = ({ city }: { city: string }) => {
  const airQualityInfoArray = useOpenAQ(city);
  const [selectedPollutant, setSelectedPollutant] = useState(undefined);
  const [selectedAirQualityInfoIndex, setSelectedAirQualityInfoIndex] =
    useState(-1);
  const airQualityInfo = useDropDown({
    items: airQualityInfoArray,
    selectedIndex: selectedAirQualityInfoIndex,
  });
  const filteredAqMeasurement = usePollutantSelection({
    items: airQualityInfo?.measurements,
    selectedPollutant: selectedPollutant,
  });
  const [compareList, setCompareList] = useState<IAirQuality[]>([]);

  if (!airQualityInfoArray || isEmpty(airQualityInfoArray))
    return (
      <MessageBox
        header={`City: ${city}`}
        message={"Not Available! Please enter a different city."}
      />
    );

  const cityAreaSelectionOptions: DropdownItemProps[] = mapIndexed(
    (airQualityInfo: any, index: number) => ({
      key: index,
      text: `${airQualityInfo.location}, ${airQualityInfo.city}`,
      value: index,
    })
  )(airQualityInfoArray);

  const buttonValues = map(
    (aqMeasurement: IAQMeasurement) => aqMeasurement.parameter
  )(airQualityInfo?.measurements ?? []);

  //TODO: find min and max aqiIndices for Gauge chart
  //TODO: refactor: rename variables and break-down into smaller components
  //TODO: fix: unexpected app reload
  //TODO: add: type

  return (
    <Grid columns={2}>
      <Grid.Row>
        <Grid.Column>
          <CityAreaSelection
            options={cityAreaSelectionOptions}
            handleDropdownSelectedItemIndex={(index: number) =>
              setSelectedAirQualityInfoIndex(index)
            }
          />
        </Grid.Column>
        <Grid.Column>
          <PollutantSelection
            buttonValues={buttonValues}
            setSelectedPollutant={setSelectedPollutant}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <AirQualityCompareGraph airQualitys={compareList} />
        </Grid.Column>
        <Grid.Column>
          <AirQualityInfo
            aqMeasurement={filteredAqMeasurement}
            aqiIndices={{ min: 0, max: 500 }}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <AirQualityCompareButton
            onCompare={() => {
              airQualityInfo &&
                setCompareList([...compareList, airQualityInfo]);
            }}
            onClearAll={() => {
              setCompareList([]);
            }}
          />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default AirQualityPanel;
