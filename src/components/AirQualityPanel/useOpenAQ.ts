import { useState, useEffect } from "react";
import axios from "axios";
import { IAirQuality } from "../AirQualityInfo/types";

const API_ENDPOINT = "https://docs.openaq.org/v2/latest";
const country_id = "US";

const fetchData = async (city: string) => {
  try {
    const { status, statusText, data } = await axios.get(
      `${API_ENDPOINT}?&country_id=${country_id}&city=${city}`
    );

    if (status !== 200) throw `statusText: ${statusText}`;

    return data.results as IAirQuality[];
  } catch (err) {
    console.log(err);
  }
};

const useOpenAQ = (city: string) => {
  const [state, setState] = useState<IAirQuality[] | undefined>(undefined);

  useEffect(() => {
    (async () => {
      const data = await fetchData(city);
      setState(data);
    })();
  }, [city]);

  return state;
};

export default useOpenAQ;
