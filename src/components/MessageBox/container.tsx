import { Grid, Message } from "semantic-ui-react";

interface IMessageBox {
  header: string;
  message: string;
}

const messageBoxStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const MessageBox = ({ header, message }: IMessageBox) => (
  <Grid columns={1} verticalAlign="middle" centered style={{ height: "100%" }}>
    <Grid.Row>
      <Grid.Column style={messageBoxStyle}>
        <Message compact warning>
          <Message.Header>{header}</Message.Header>
          <p>{message}</p>
        </Message>
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

export default MessageBox;
