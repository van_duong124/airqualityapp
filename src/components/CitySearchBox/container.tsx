import { useState, Dispatch, SetStateAction } from "react";
import { Input } from "semantic-ui-react";

interface ICitySearchBox {
  onSearchComplete: Dispatch<SetStateAction<string>>;
}

const handleInputChange =
  (setInput: Dispatch<SetStateAction<string>>) => (event: any) => {
    setInput(event.target.value);
  };

const CitySearchBox = ({ onSearchComplete }: ICitySearchBox) => {
  const [input, setInput] = useState("");
  return (
    <Input
      action={{
        content: "Search",
        onClick: () => onSearchComplete(input),
      }}
      onChange={handleInputChange(setInput)}
      defaultValue={input}
      placeholder="Search a city..."
    />
  );
};

export default CitySearchBox;
