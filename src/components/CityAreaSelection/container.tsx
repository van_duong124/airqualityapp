import { head } from "ramda";
import { Dropdown, DropdownItemProps } from "semantic-ui-react";

interface ICityAreaSelection {
  options: DropdownItemProps[];
  handleDropdownSelectedItemIndex: (index: number) => void;
}

const onDropBoxValueChange =
  (handleDropdownSelectedItem: (index: number) => void) =>
  (event: any, data: any) => {
    handleDropdownSelectedItem(data.value);
  };

const CityAreaSelection = ({
  options,
  handleDropdownSelectedItemIndex,
}: ICityAreaSelection) => (
  <Dropdown
    placeholder="Select a location"
    fluid
    search
    selection
    options={options}
    onChange={onDropBoxValueChange(handleDropdownSelectedItemIndex)}
    defaultValue={head(options)?.value}
  />
);

export default CityAreaSelection;
