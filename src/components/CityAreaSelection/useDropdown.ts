import { isNil } from "ramda";
import { useEffect, useState } from "react";
import { IAirQuality } from "../AirQualityInfo/types";

interface IUseDropDown {
  items: IAirQuality[] | undefined;
  selectedIndex: number;
}

const useDropDown = ({ items, selectedIndex }: IUseDropDown) => {
  const [selectedItem, setSelectedItem] =
    useState<IAirQuality | undefined>(undefined);

  useEffect(() => {
    if (!isNil(items)) {
      const selectedItem = items[selectedIndex === -1 ? 0 : selectedIndex];
      setSelectedItem(selectedItem);
    }
  }, [items, selectedIndex]);

  return selectedItem;
};

export default useDropDown;
