export type AQIType =
  | "Good"
  | "Moderate"
  | "Unhealthy for Sensitive Groups"
  | "Unhealthy"
  | "Very Unhealthy"
  | "Hazardous"
  | "Unavailable";

export type AQIColorType =
  | "green"
  | "yellow"
  | "orange"
  | "red"
  | "purple"
  | "maroon";

export interface IAQMeasurement {
  parameter: string;
  value: number;
  lastUpdated: string;
  unit: string;
}

export interface IAirQuality {
  location?: string;
  city: string;
  country: string;
  coordinates?: {
    latitude: number;
    longitude: number;
  };
  measurements: IAQMeasurement[];
}

export interface IAqiInfo {
  aqi: number;
  color: AQIColorType;
  levelOfConcern: AQIType;
  description: string;
}

export interface IAirQualityInfo {
  aqMeasurement: IAQMeasurement;
  aqiIndices: { min: number; max: number };
}
