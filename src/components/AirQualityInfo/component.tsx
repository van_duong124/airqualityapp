import { AQIColorType, IAirQualityInfo, IAqiInfo } from "./types";
import ReactSpeedometer from "react-d3-speedometer";
import { map } from "ramda";
import { aqiInfoArr } from "./constants";

const segmentColors = map(({ color }: Partial<IAqiInfo>) => color)(
  aqiInfoArr
) as AQIColorType[];

export const AirQualityInfoComp = ({
  aqMeasurement,
  aqiIndices = { min: 0, max: 500 },
}: IAirQualityInfo) => (
  <ReactSpeedometer
    width={500}
    minValue={aqiIndices.min}
    maxValue={aqiIndices.max}
    value={aqMeasurement.value}
    segments={6}
    customSegmentStops={[
      aqiIndices.min,
      50,
      100,
      150,
      200,
      300,
      aqiIndices.max,
    ]}
    segmentColors={segmentColors}
    textColor="black"
    currentValueText={`${aqMeasurement.parameter} - ${aqMeasurement.value} ${aqMeasurement.unit}`}
  />
);
