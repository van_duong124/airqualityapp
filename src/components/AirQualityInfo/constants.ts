import { IAqiInfo } from "./types";

export enum AQI_COLOR {
  GREEN = "Good",
  YELLOW = "Moderate",
  ORANGE = "Unhealthy for Sensitive Groups",
  RED = "Unhealthy",
  PURPLE = "Very Unheadlthy",
  MAROON = "Hazardous",
}

export const aqiInfoArr: IAqiInfo[] = [
  {
    aqi: 50,
    color: "green",
    levelOfConcern: "Good",
    description:
      "0 - 50: Air quality is satisfactory, and air pollution poses little or no risk",
  },
  {
    aqi: 100,
    color: "yellow",
    levelOfConcern: "Moderate",
    description:
      "51 - 100: Air quality is acceptable. However, there may be a risk for some people, particularly those who are unusually sensitive to air pollution",
  },
  {
    aqi: 150,
    color: "orange",
    levelOfConcern: "Unhealthy for Sensitive Groups",
    description:
      "101 - 150: Members of sensitive groups may experience health effects. The general public is less likely to be affected",
  },
  {
    aqi: 200,
    color: "red",
    levelOfConcern: "Unhealthy",
    description:
      "151 - 200: Some members of the general public may experience health effects; members of sensitive groups may experience more serious health effects",
  },
  {
    aqi: 300,
    color: "purple",
    levelOfConcern: "Very Unhealthy",
    description:
      "201 - 300: Health alert: The risk of health effects is increased for everyone",
  },
  {
    aqi: 400,
    color: "maroon",
    levelOfConcern: "Hazardous",
    description:
      "301 and higher: Health warning of emergency conditions: everyone is more likely to be affected",
  },
];
