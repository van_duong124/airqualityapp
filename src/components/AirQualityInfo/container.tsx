import { AirQualityInfoComp } from "./component";
import { IAQMeasurement } from "./types";

interface IAirQualityInfo {
  aqMeasurement: IAQMeasurement | undefined;
  aqiIndices: { min: number; max: number };
}

const AirQualityInfo = ({ aqMeasurement, aqiIndices }: IAirQualityInfo) => (
  <>
    {aqMeasurement && (
      <AirQualityInfoComp
        aqMeasurement={aqMeasurement}
        aqiIndices={aqiIndices}
      />
    )}
  </>
);

export default AirQualityInfo;
