import { addIndex, length, map } from "ramda";
import { Button, Popup, SemanticWIDTHS } from "semantic-ui-react";
import { aqiInfoArr } from "../AirQualityInfo/constants";

const mapIndexed = addIndex(map);

const BottomAQIInfoBarStyle = {
  position: "absolute",
  left: 0,
  bottom: 0,
  width: "100%",
  backgroudColor: "black",
};

const buttonGroupLen = (length(aqiInfoArr) + 1) as SemanticWIDTHS;

const BottomAQIInfoBar = ({}) => (
  <Button.Group widths={buttonGroupLen} style={BottomAQIInfoBarStyle}>
    <Button>Air Quality Scale</Button>
    {mapIndexed((aqiInfo: any, index: number) => (
      <Popup
        content={aqiInfo.description}
        trigger={
          <Button
            key={index}
            width={70}
            style={{ backgroundColor: aqiInfo.color, color: "black" }}
          >
            {aqiInfo.levelOfConcern}
          </Button>
        }
      />
    ))(aqiInfoArr)}
  </Button.Group>
);

export default BottomAQIInfoBar;
