import { filter, head } from "ramda";
import { useEffect, useState } from "react";
import { IAQMeasurement } from "../AirQualityInfo/types";

interface IUsePollutantSelection {
  items: IAQMeasurement[] | undefined;
  selectedPollutant: string | undefined;
}

const usePollutantSelection = ({
  items,
  selectedPollutant,
}: IUsePollutantSelection) => {
  const [selectedItem, setSelectedItem] =
    useState<IAQMeasurement | undefined>(undefined);

  useEffect(() => {
    if (selectedPollutant && items) {
      const filteredItems = filter(
        (item: IAQMeasurement) => item.parameter === selectedPollutant
      )(items);

      setSelectedItem(head(filteredItems));
    }
  }, [items, selectedPollutant]);

  return selectedItem;
};

export default usePollutantSelection;
