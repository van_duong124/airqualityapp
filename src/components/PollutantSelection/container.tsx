import { map } from "ramda";
import { Button } from "semantic-ui-react";

const onSelectedPollutant =
  (setSelectedPollutant: any) => (event: any, data: any) => {
    const selectedPollutant = event.target.value;
    setSelectedPollutant(selectedPollutant);
  };

interface IPollutantSelection {
  buttonValues: string[];
  setSelectedPollutant: any;
}

const PollutantSelection = ({
  buttonValues,
  setSelectedPollutant,
}: IPollutantSelection) => {
  return (
    <Button.Group floated="left">
      {map((value: string) => (
        <Button
          key={value}
          onClick={onSelectedPollutant(setSelectedPollutant)}
          value={value}
        >
          {value}
        </Button>
      ))(buttonValues)}
    </Button.Group>
  );
};

export default PollutantSelection;
