import {
  flatten,
  map,
  mapObjIndexed,
  mergeAll,
  pipe,
  reduce,
  uniq,
  values,
} from "ramda";
import { Bar } from "react-chartjs-2";
import { IAirQuality, IAQMeasurement } from "../AirQualityInfo/types";

interface IAirQualityCompareGraph {
  airQualitys: IAirQuality[];
}

const randomColor = () => Math.floor(Math.random() * 16777215).toString(16);

const barOptions = {
  scales: {
    yAxes: [
      {
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
      },
    ],
    xAxes: [
      {
        stacked: true,
      },
    ],
  },
};

const AirQualityCompareGraph = ({ airQualitys }: IAirQualityCompareGraph) => {
  const getPollutantsStr = pipe(
    map((item: IAirQuality) =>
      map((measurement: IAQMeasurement) => measurement.parameter)(
        item.measurements
      )
    ),
    flatten,
    uniq
  )(airQualitys);

  const initObjOfMeasurementValueArr = pipe(
    map((item: any) => ({ [item]: [] })),
    mergeAll
  )(getPollutantsStr); // {"co": [], "pm25": []}

  const objOfMeasurementValueArr = reduce((acc: any, curr: any) => {
    curr.measurements.forEach(
      (item: any) =>
        (acc[item.parameter] = [...acc[item.parameter], item.value])
    );
    return acc;
  }, initObjOfMeasurementValueArr)(airQualitys); // {"co": [], "pm25": []}

  const transformObjOfMeasurementValueArr = mapObjIndexed(
    (value, key, obj) => ({
      label: key,
      data: value,
      backgroundColor: `#${randomColor()}`,
    })
  )(objOfMeasurementValueArr); // {"co": {label: "label", "data": data, backgroundColor: color}}

  const datasets = values(transformObjOfMeasurementValueArr);

  const barData = {
    labels: map((item: IAirQuality) => `${item.location}, ${item.city}`)(
      airQualitys
    ),
    datasets: datasets,
  };

  return <Bar type="bar" data={barData} options={barOptions} />;
};

export default AirQualityCompareGraph;
